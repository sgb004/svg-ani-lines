declare module 'svg-ani-lines' {
	export class SVGAniLines {}

	export class SVGAniLinesBase {
		/**
		 * Constructor
		 *
		 * @param svg - SVG element to animate (optional)
		 */
		constructor(svg?: SVGSVGElement);

		/**
		 * Adds animation to paths and circles in the SVG
		 *
		 * @param svg - SVG element to animate
		 */
		add(svg: SVGSVGElement): void;

		/**
		 * Removes previously added animations
		 *
		 * @param svg - SVG element to animate
		 */
		remove(svg: SVGSVGElement): void;
	}
}
