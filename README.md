# SVG Ani Lines

[![Version](https://img.shields.io/badge/version-1.0.7-blue.svg)](https://www.npmjs.com/package/sgb004-svg-ani-lines)
[![Author](https://img.shields.io/badge/author-sgb004-green.svg)](https://sgb004.com)

With this library you can add animation to SVG elements. The lines in the SVG will redraw on hover.

![Examples](https://svg-ani-lines.sgb004.com/examples.gif)

<!-- Examples -->

## Installation

To install **SVG Ani lines** you can use npm:

```node
npm i sgb004-svg-ani-lines
```

## Usage

### Web

You can download `svg-ani-lines.js` and `svg-ani-lines.css` from the dist directory and add them to your HTML.
Then, add the `ani-svg-lines` class to the SVG you want to apply the animation:

```html
<link rel="stylesheet" href="svg-ani-lines.css" />
<script src="svg-ani-lines.js"></script>

<svg class="svg-ani-lines">
	<!-- SVG content -->
</svg>
```

You can disable the animation when hover is used on the SVG by adding the `disable-hover` class:

```html
<svg class="svg-ani-lines disable-hover">
	<!-- SVG content -->
</svg>
```

To enable the animation when using the `disable-hover` class you can use the `fill` class:

```html
<svg class="svg-ani-lines disable-hover fill">
	<!-- SVG content -->
</svg>
```

[Check examples](https://svg-ani-lines.sgb004.com/web)

### React

In React you can import the class `SVGAniLinesBase`:

```js
import SVGAniLinesBase from 'sgb004-SVGAniLinesBase';

useEffect(() => {
	const svgAniLines = new SVGAniLinesBase(yourSvg);
	//or
	const svgAniLines = new SVGAniLinesBase();
	svgAniLines.add(yourSvg);

	return () => {
		svgAniLines.remove(yourSvg);
	};
}, []);
```

Or you can import the `SVGAniLines` component:

<!-- prettier-ignore -->
```jsx
import SVGAniLines from 'sgb004-SVGAniLines';

<SVGAniLines>
	{/* SVG content */}
</SVGAniLines>;
```

You can disable the animation when hover is used on the SVG by adding the `disable-hover` class:

<!-- prettier-ignore -->
```jsx
import SVGAniLines from 'sgb004-SVGAniLines';

<SVGAniLines className="disable-hover">
	{/* SVG content */}
</SVGAniLines>;
```

To enable the animation when using the `disable-hover` class you can use the `fill` class:

<!-- prettier-ignore -->
```jsx
import SVGAniLines from 'sgb004-SVGAniLines';

<SVGAniLines  className="disable-hover fill">
	{/* SVG content */}
</SVGAniLines>;
```

[Check examples](https://svg-ani-lines.sgb004.com/react)

### Webpack

In webpack you can import the class `SVGAniLinesBase`:

```js
const svgAniLines = new SVGAniLinesBase(yourSvg);
//or
const svgAniLines = new SVGAniLinesBase();
svgAniLines.add(yourSvg);

//To remove the animation
svgAniLines.remove(yourSvg);
```

## API

### SVGAniLinesBase

#### Constructor

-   `svg` - (Optional) The SVG to add animation.

#### add

-   `svg` - Adds animation to the given SVG.

#### remove

-   `svg` - Removes animation from the given SVG.

## Author

[sgb004](https://sgb004.com)

## License

MIT
