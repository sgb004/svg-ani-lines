import React from 'react';
import ReactDOM from 'react-dom/client';
import Readme from './Readme';
import './index.css';

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
root.render(
	<React.StrictMode>
		<div className="content">
			<Readme />
		</div>
	</React.StrictMode>
);
