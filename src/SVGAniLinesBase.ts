class SVGAniLinesBase {
	constructor(svg: SVGSVGElement | null = null) {
		if (svg) {
			this.add(svg);
		}
	}

	add(svg: SVGSVGElement) {
		const paths = svg.querySelectorAll('path, circle') as NodeListOf<
			SVGPathElement | SVGCircleElement
		>;

		for (let i: number = 0; i < paths.length; i++) {
			this._addPathCloned(paths[i]);
		}
	}

	remove(svg: SVGSVGElement) {
		const cloned = svg.querySelectorAll('.animated') as NodeListOf<
			SVGPathElement | SVGCircleElement
		>;

		for (let i: number = 0; i < cloned.length; i++) {
			this._removePathCloned(cloned[i]);
		}
	}

	_addPathCloned(path: SVGPathElement | SVGCircleElement) {
		const parent: ParentNode | null = path.parentNode;
		const cloned = path.cloneNode(true) as SVGPathElement | SVGCircleElement;

		cloned.classList.add('animated');
		cloned.style.setProperty('--stroke-dash-value', `${Math.ceil(path.getTotalLength())}`);

		if (parent) {
			parent.appendChild(cloned);
		}
	}

	_removePathCloned(path: SVGPathElement | SVGCircleElement) {
		const parent: ParentNode | null = path.parentNode;

		if (parent) {
			parent.removeChild(path);
		}
	}
}

export default SVGAniLinesBase;
