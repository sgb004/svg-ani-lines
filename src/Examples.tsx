import { useState } from 'react';

import SVGAniLines from './SVGAniLines';

const Examples = () => {
	let [disableHover, setDisableHover] = useState(false);
	let [fill, setFill] = useState(false);

	return (
		<div id="examples">
			<h2>Examples</h2>
			<SVGAniLines
				version="1.1"
				viewBox="0 0 32 32"
				xmlns="http://www.w3.org/2000/svg"
				className={`${disableHover ? 'disable-hover' : ''} ${
					disableHover && fill ? 'fill' : ''
				}`}
			>
				<g fill="none" strokeWidth="2" stroke="#000" strokeLinecap="round">
					<path d="M13.533 30.775A15 15 0 0016 31c8.284 0 15-6.716 15-15 0-8.284-6.716-15-15-15C7.716 1 1 7.716 1 16a15 15 0 008.31 13.387" />
					<path d="M15.717 22.319v-.101m0-2.934c.074-1.9.5-2.914 2.007-4.301.734-.687 1.294-1.514 1.294-2.54 0-1.574-1.294-2.661-2.84-2.661-2.161 0-3.235 1.093-3.195 3.094" />
				</g>
			</SVGAniLines>

			<SVGAniLines
				viewBox="0 0 32 32"
				xmlns="http://www.w3.org/2000/svg"
				className={`${disableHover ? 'disable-hover' : ''} ${
					disableHover && fill ? 'fill' : ''
				}`}
			>
				<g fill="none" strokeWidth="2">
					<path
						d="M13.462 31.182c8.878 1.493 17.137-4.922 17.89-13.899.755-8.976-6.318-16.68-15.321-16.69C4.401.586-3.035 12.992 2.45 23.255L.638 29.902c-.122.424.001.88.32 1.185.235.215.546.326.864.308a.994.994 0 00.32 0L9.14 29.44"
						strokeLinecap="round"
						transform="matrix(.97516 0 0 .97432 .397 .41)"
					/>
					<path
						d="M15.056 11.353 l-1.121-1.55a1.99 1.99 0 00-1.45-.814 2.008 2.008 0 00-1.566.581l-1.16 1.163a2.725 2.725 0 00-.773 2.015c0 1.714 1.314 3.874 3.866 6.393 2.987 2.906 5.055 3.874 6.467 3.874a2.607 2.607 0 001.933-.784l1.16-1.163a1.94 1.94 0 00-.232-3.012l-1.518-1.095a2.017 2.017 0 00-1.769-.29l-1.44.455a.512.512 0 01-.251-.087 11.049 11.049 0 01-2.272-2.19.298.298 0 010-.106c.126-.426.339-1.114.484-1.589a1.94 1.94 0 00-.358-1.801z"
						transform="matrix(.97516 0 0 .97432 .397 .41)"
					/>
				</g>
			</SVGAniLines>

			<SVGAniLines
				version="1.1"
				viewBox="0 0 32 32"
				xmlns="http://www.w3.org/2000/svg"
				className={`${disableHover ? 'disable-hover' : ''} ${
					disableHover && fill ? 'fill' : ''
				}`}
			>
				<g fill="none" strokeWidth="2" stroke="#000" strokeLinecap="round">
					<path d="M29.747 8.632l-11.93 9.69a2.832 2.832 0 01-3.602 0L2.253 8.633" />
					<path d="M29.747 20.66V7.433c0-.646-.532-1.139-1.158-1.139H3.443c-.658 0-1.159.523-1.159 1.139v17.136c0 .646.532 1.139 1.159 1.139h26.304" />
				</g>
			</SVGAniLines>

			<SVGAniLines
				version="1.1"
				viewBox="0 0 32 32"
				xmlns="http://www.w3.org/2000/svg"
				className={`${disableHover ? 'disable-hover' : ''} ${
					disableHover && fill ? 'fill' : ''
				}`}
			>
				<g fill="none" strokeWidth="3" stroke="#000" strokeLinecap="round">
					<path d="M30.807 1.193L17.957 14a3.05 3.743 0 01-3.88 0L1.193 1.194M30.807 30.807L17.957 18a3.05 3.743 0 00-3.88 0L1.193 30.806" />
				</g>
			</SVGAniLines>

			<SVGAniLines
				preserveAspectRatio="none"
				xmlns="http://www.w3.org/2000/svg"
				viewBox="0 0 32 32"
				className={`${disableHover ? 'disable-hover' : ''} ${
					disableHover && fill ? 'fill' : ''
				}`}
			>
				<g fill="#16171b" strokeWidth="1.5">
					<circle cx="15.996" cy="27.242" r="2.783" />
					<path d="M28.52 11.353a2.772 2.772 0 01-1.473-.418 20.855 20.855 0 00-22.096 0C1.82 12.81-1.06 8.198 1.998 6.21a26.421 26.421 0 0127.996 0c2.375 1.48 1.324 5.146-1.475 5.144z" />
					<path d="M23.093 21.106a2.76 2.76 0 01-1.577-.493 9.863 9.863 0 00-11.034 0c-3.068 2.18-6.288-2.49-3.159-4.582a15.271 15.271 0 0117.352 0c2.25 1.55 1.15 5.078-1.582 5.075z" />
				</g>
			</SVGAniLines>

			<div>
				<button
					onClick={() => {
						setDisableHover(!disableHover);
					}}
				>
					{disableHover
						? 'Press me to use hover'
						: 'Press me to disable fill while hover is used'}
				</button>
				{disableHover ? (
					<button
						onClick={() => {
							setFill(!fill);
						}}
					>
						{fill ? 'Press me to drain the icons' : 'Press me to fill the icons'}
					</button>
				) : (
					<></>
				)}
			</div>
		</div>
	);
};

export default Examples;
