import Examples from './Examples';

const Readme = () => (
	<div id="readme">
	<h1>SVG Ani Lines</h1>
<p><a href="https://www.npmjs.com/package/sgb004-svg-ani-lines"><img src="https://img.shields.io/badge/version-1.0.7-blue.svg" alt="Version" /></a>
<a href="https://sgb004.com"><img src="https://img.shields.io/badge/author-sgb004-green.svg" alt="Author" /></a></p>
<p>With this library you can add animation to SVG elements. The lines in the SVG will redraw on hover.</p>
<p><img src="https://svg-ani-lines.sgb004.com/examples.gif" alt="Examples" /></p>
<Examples />
<h2>Installation</h2>
<p>To install <strong>SVG Ani lines</strong> you can use npm:</p>
<pre><code>npm i sgb004-svg-ani-lines<br/></code></pre>
<h2>Usage</h2>
<h3>Web</h3>
<p>You can download <code>svg-ani-lines.js</code> and <code>svg-ani-lines.css</code> from the dist directory and add them to your HTML.
Then, add the <code>ani-svg-lines</code> class to the SVG you want to apply the animation:</p>
<pre><code>&lt;link rel="stylesheet" href="svg-ani-lines.css" /&gt;<br/>&lt;script src="svg-ani-lines.js"&gt;&lt;/script&gt;<br/><br/>&lt;svg class="svg-ani-lines"&gt;<br/>    &lt;!-- SVG content --&gt;<br/>&lt;/svg&gt;<br/></code></pre>
<p>You can disable the animation when hover is used on the SVG by adding the <code>disable-hover</code> class:</p>
<pre><code>&lt;svg class="svg-ani-lines disable-hover"&gt;<br/>    &lt;!-- SVG content --&gt;<br/>&lt;/svg&gt;<br/></code></pre>
<p>To enable the animation when using the <code>disable-hover</code> class you can use the <code>fill</code> class:</p>
<pre><code>&lt;svg class="svg-ani-lines disable-hover fill"&gt;<br/>    &lt;!-- SVG content --&gt;<br/>&lt;/svg&gt;<br/></code></pre>
<p><a href="https://svg-ani-lines.sgb004.com/web">Check examples</a></p>
<h3>React</h3>
<p>In React you can import the class <code>SVGAniLinesBase</code>:</p>
<pre><code>import SVGAniLinesBase from 'sgb004-SVGAniLinesBase';<br/><br/>useEffect(() =&gt; &#123;<br/>    const svgAniLines = new SVGAniLinesBase(yourSvg);<br/>    //or<br/>    const svgAniLines = new SVGAniLinesBase();<br/>    svgAniLines.add(yourSvg);<br/><br/>    return () =&gt; &#123;<br/>        svgAniLines.remove(yourSvg);<br/>    &#125;;<br/>&#125;, []);<br/></code></pre>
<p>Or you can import the <code>SVGAniLines</code> component:</p>
{/* prettier-ignore */}
<pre><code>import SVGAniLines from 'sgb004-SVGAniLines';<br/><br/>&lt;SVGAniLines&gt;<br/>    &#123;/* SVG content */&#125;<br/>&lt;/SVGAniLines&gt;;<br/></code></pre>
<p>You can disable the animation when hover is used on the SVG by adding the <code>disable-hover</code> class:</p>
{/* prettier-ignore */}
<pre><code>import SVGAniLines from 'sgb004-SVGAniLines';<br/><br/>&lt;SVGAniLines className="disable-hover"&gt;<br/>    &#123;/* SVG content */&#125;<br/>&lt;/SVGAniLines&gt;;<br/></code></pre>
<p>To enable the animation when using the <code>disable-hover</code> class you can use the <code>fill</code> class:</p>
{/* prettier-ignore */}
<pre><code>import SVGAniLines from 'sgb004-SVGAniLines';<br/><br/>&lt;SVGAniLines  className="disable-hover fill"&gt;<br/>    &#123;/* SVG content */&#125;<br/>&lt;/SVGAniLines&gt;;<br/></code></pre>
<p><a href="https://svg-ani-lines.sgb004.com/react">Check examples</a></p>
<h3>Webpack</h3>
<p>In webpack you can import the class <code>SVGAniLinesBase</code>:</p>
<pre><code>const svgAniLines = new SVGAniLinesBase(yourSvg);<br/>//or<br/>const svgAniLines = new SVGAniLinesBase();<br/>svgAniLines.add(yourSvg);<br/><br/>//To remove the animation<br/>svgAniLines.remove(yourSvg);<br/></code></pre>
<h2>API</h2>
<h3>SVGAniLinesBase</h3>
<h4>Constructor</h4>
<ul>
<li><code>svg</code> - (Optional) The SVG to add animation.</li>
</ul>
<h4>add</h4>
<ul>
<li><code>svg</code> - Adds animation to the given SVG.</li>
</ul>
<h4>remove</h4>
<ul>
<li><code>svg</code> - Removes animation from the given SVG.</li>
</ul>
<h2>Author</h2>
<p><a href="https://sgb004.com">sgb004</a></p>
<h2>License</h2>
<p>MIT</p>
	</div>
);

export default Readme;
	