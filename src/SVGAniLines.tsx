import React, { RefObject } from 'react';

import './svg-ani-lines.css';
import SVGAniLinesBase from './SVGAniLinesBase';

class SVGAniLines extends React.Component<React.SVGProps<SVGSVGElement>> {
	_svg: RefObject<SVGSVGElement>;
	_svgAniLinesBase: SVGAniLinesBase;

	constructor(props: React.SVGProps<SVGSVGElement>) {
		super(props);
		this._svg = React.createRef();
		this._svgAniLinesBase = new SVGAniLinesBase();
	}

	componentDidMount() {
		if (!this._svg.current) return;

		this._svgAniLinesBase.add(this._svg.current);
	}

	componentWillUnmount() {
		if (!this._svg.current) return;

		this._svgAniLinesBase.remove(this._svg.current);
	}

	render() {
		const props = {
			...this.props,
			className: `svg-ani-lines ${this.props.className}`.trim(),
		};

		return (
			<svg {...props} ref={this._svg}>
				{this.props.children}
			</svg>
		);
	}
}

export default SVGAniLines;
