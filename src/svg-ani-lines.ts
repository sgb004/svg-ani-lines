import SVGAniLinesBase from './SVGAniLinesBase';

(() => {
	const ready = () => {
		const svg = document.querySelectorAll('.svg-ani-lines') as NodeListOf<SVGSVGElement>;

		for (let i: number = 0; i < svg.length; i++) {
			new SVGAniLinesBase(svg[i] as SVGSVGElement);
		}
	};

	document.addEventListener('DOMContentLoaded', ready);
})();
