class SVGAniLinesBase {
    constructor(svg = null) {
        if (svg) {
            this.add(svg);
        }
    }
    add(svg) {
        const paths = svg.querySelectorAll('path, circle');
        for (let i = 0; i < paths.length; i++) {
            this._addPathCloned(paths[i]);
        }
    }
    remove(svg) {
        const cloned = svg.querySelectorAll('.animated');
        for (let i = 0; i < cloned.length; i++) {
            this._removePathCloned(cloned[i]);
        }
    }
    _addPathCloned(path) {
        const parent = path.parentNode;
        const cloned = path.cloneNode(true);
        cloned.classList.add('animated');
        cloned.style.setProperty('--stroke-dash-value', `${Math.ceil(path.getTotalLength())}`);
        if (parent) {
            parent.appendChild(cloned);
        }
    }
    _removePathCloned(path) {
        const parent = path.parentNode;
        if (parent) {
            parent.removeChild(path);
        }
    }
}
export default SVGAniLinesBase;
