function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }
import React from 'react';
import './svg-ani-lines.css';
import SVGAniLinesBase from './SVGAniLinesBase';
class SVGAniLines extends React.Component {
  constructor(props) {
    super(props);
    this._svg = /*#__PURE__*/React.createRef();
    this._svgAniLinesBase = new SVGAniLinesBase();
  }
  componentDidMount() {
    if (!this._svg.current) return;
    this._svgAniLinesBase.add(this._svg.current);
  }
  componentWillUnmount() {
    if (!this._svg.current) return;
    this._svgAniLinesBase.remove(this._svg.current);
  }
  render() {
    const props = Object.assign(Object.assign({}, this.props), {
      className: `svg-ani-lines ${this.props.className}`.trim()
    });
    return /*#__PURE__*/React.createElement("svg", _extends({}, props, {
      ref: this._svg
    }), this.props.children);
  }
}
export default SVGAniLines;